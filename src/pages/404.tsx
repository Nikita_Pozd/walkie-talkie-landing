import { FC } from 'react';
import { MainLayout } from '../layouts/main.layout';
import { Seo } from '../components/seo';
import { PageProps } from 'gatsby';

const NotFoundPage: FC<PageProps> = () => (
  <MainLayout>
    <Seo title="404: Not found" />
    <h1>404: Not Found</h1>
    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </MainLayout>
);

export default NotFoundPage;
