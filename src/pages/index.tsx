import { FC } from 'react';
import { PageProps } from 'gatsby';
import { MainLayout } from '../layouts/main.layout';
import { Seo } from '../components/seo';
import { Parallax } from '../components/parallax';
import { AboutProject } from '../components/aboutProject';

const IndexPage: FC<PageProps> = () => (
  <MainLayout>
    <Seo title="Главная" />
    <Parallax />
    <AboutProject />
  </MainLayout>
);

export default IndexPage;
