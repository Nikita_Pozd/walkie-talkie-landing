import { FC } from 'react';
import styles from './styles.module.scss';
import { Typography } from 'antd';

const { Title, Paragraph } = Typography;

export const Parallax: FC = () => {
  return (
    <section className={styles.parallax}>
      <div className={styles.parallax__content}>
        <Title className={styles.parallax__header}>Walkie Talkie</Title>
        <Paragraph className={styles.parallax__description}>
          Приложение для общения и новых знакомств
        </Paragraph>
      </div>
    </section>
  );
};
