import { FC } from 'react';
import { PageHeader, Button, Dropdown, Tag } from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import { Link } from 'gatsby';
import { Menu } from 'antd';
import styles from './styles.module.scss';
interface HeaderProps {
  siteTitle: string;
}

const menu = (
  <Menu>
    <Menu.Item>
      <Link to="#1" className={styles.pageHeader__linkItem}>
        О проекте
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="#2" className={styles.pageHeader__linkItem}>
        Задачи
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="#3" className={styles.pageHeader__linkItem}>
        Разработчики
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="#4" className={styles.pageHeader__linkItem}>
        Технологии
      </Link>
    </Menu.Item>
  </Menu>
);

const DropdownMenu = () => (
  <Dropdown key="more" overlay={menu}>
    <Button
      style={{
        border: 'none',
        padding: 0,
      }}
    >
      <EllipsisOutlined
        style={{
          fontSize: 20,
          verticalAlign: 'top',
        }}
      />
    </Button>
  </Dropdown>
);

export const Header: FC<HeaderProps> = ({ siteTitle }) => {
  return (
    <PageHeader
      className={styles.pageHeader}
      title={siteTitle}
      backIcon={false}
      tags={<Tag color="blue">В разработке</Tag>}
      extra={[
        <Button key="1">GitHub</Button>,
        <Button key="2" type="primary">
          Сайт
        </Button>,
        <DropdownMenu key="3" />,
      ]}
    />
  );
};
