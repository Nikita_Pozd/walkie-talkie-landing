import { FC } from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { Header } from '../../components/header';
import styles from './styles.module.scss';

interface MainLayoutProps {
  children: React.ReactNode;
}

export const MainLayout: FC<MainLayoutProps> = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <div className={styles.layoutConatiner}>
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
      <main>{children}</main>
      <footer></footer>
    </div>
  );
};
